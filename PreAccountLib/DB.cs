﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Dapper;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace PreAccount_DataExtract
{
    public static class DBLocal
    {
        public static Func<DataAccessLocal> ConnectionFactory = () => new DataAccessLocal(new SqlConnection(ConnectionString.ConnectionLocal));

        
    }
    public static class DBIndfak
    {
        public static Func<DataAccessIndfak> ConnectionFactory = () => new DataAccessIndfak(new SqlConnection(ConnectionString.ConnectionIndfak));

    }
    public static class ConnectionString
    {
        public static string ConnectionLocal = ConfigurationManager.AppSettings["connectionStringLocal"];
        public static string ConnectionIndfak = ConfigurationManager.AppSettings["connectionStringIndfak"];
    }

    public class DataAccessIndfak : IDisposable
    {
        private int _timeout = 60 * 10;

        public DbConnection Context { get; private set; }
        public DbTransaction _currenttrans { get; private set; }

        public DataAccessIndfak(DbConnection context) { this.Context = context; }


        internal IList<string> GetEANs(long customerid)
        {
            var parms = new DynamicParameters();
            parms.Add(name: "@CustomerId", value: customerid, direction: System.Data.ParameterDirection.Input);
            return Context.Query<string>(sql: SQL.GetEANs, param: parms, transaction: _currenttrans, commandTimeout: _timeout).ToList();
        }
            


        internal DimensionModel GetDimensionModel(long customerid)
        {
            var model = new DimensionModel();
            var parms = new DynamicParameters();
            parms.Add(name: "@CustomerId", value: customerid, direction: System.Data.ParameterDirection.Input);
            using (var multi = Context.QueryMultiple(sql: SQL.GetDimensionModelData, param: parms, transaction: _currenttrans, commandTimeout: _timeout, commandType: System.Data.CommandType.Text))
            {
                model = multi.Read<DimensionModel>().FirstOrDefault();
                model.Codes = multi.Read<DimensionCode>().ToDictionary(r => r.CodeName, r => r);
                var values = multi.Read<DimensionValue>().ToLookup(r => r.DimensionCodeId, r=>r);
                model.OrganisationName = multi.Read<string>().FirstOrDefault();

                foreach (var c in model.Codes.Values)
                {
                    c.Values = values[c.Id].ToDictionary(r => r.Value, r => r);
                    foreach (var v in c.Values) { v.Value.DimensionCode = c; }
                }
            }

            return model; 
        }
        public void Open() { Context.Open(); }
        public DbTransaction BeginTransaction()
        {
            return BeginTransaction(System.Data.IsolationLevel.Snapshot);
        }
        public DbTransaction BeginTransaction(System.Data.IsolationLevel level)
        {
            _currenttrans = Context.BeginTransaction(level);
            return _currenttrans;
        }
        public void Close() { try { Context.Close(); } catch (Exception) { } }

        public void Dispose()
        {
            if (Context != null) Context.Dispose();
        }
    }


    public class DataAccessLocal : IDisposable
    {
        private int _timeout = 60 * 10;

        public DbConnection Context { get; private set; }
        public DbTransaction _currenttrans { get; private set; }

        public DataAccessLocal(DbConnection context) { this.Context = context; }

        public void Open() { Context.Open(); }
        public DbTransaction BeginTransaction()
        {
            return BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
        }
        public DbTransaction BeginTransaction(System.Data.IsolationLevel level)
        {
            _currenttrans = Context.BeginTransaction(level);
            return _currenttrans;
        }


        internal void SaveData(long ic, long ics, long customerid,IList<Model> models, ModelData modeldata, DimensionModel dimensionmodel)
        {
            DynamicParameters parms;
            Console.WriteLine(ic + "/" + ics + ", " + customerid + ", " + DateTime.Now.ToLongTimeString() + ", Model");

            if (modeldata.Id == 0)
            {
                parms = new DynamicParameters();
                parms.Add(name: "@CustomerId", value: customerid, direction: System.Data.ParameterDirection.Input);
                parms.Add(name: "@OrgName", value: dimensionmodel.OrganisationName, direction: System.Data.ParameterDirection.Input);
                parms.Add(name: "@Start", value: modeldata.Start, direction: System.Data.ParameterDirection.Input);
                parms.Add(name: "@End", value: modeldata.End, direction: System.Data.ParameterDirection.Input);
                parms.Add(name: "@DimensionModelId", value: modeldata.DimensionModelId, direction: System.Data.ParameterDirection.Input);
                modeldata.Id = Context.ExecuteScalar<long>(sql: SQL.SaveRun, param: parms, transaction: _currenttrans, commandTimeout: _timeout);
            }
            Console.WriteLine(ic + "/" + ics + ", " + customerid + ", " + DateTime.Now.ToLongTimeString() + ", Headers: " + modeldata.Headers.Values.Count);

            foreach (var header in modeldata.Headers.Values)
            {
                if (header.Id == 0)
                {
                    parms = new DynamicParameters();
                    parms.Add(name: "@DataImportId", value: modeldata.Id, direction: System.Data.ParameterDirection.Input);
                    parms.Add(name: "@Name", value: header.Name, direction: System.Data.ParameterDirection.Input);
                    parms.Add(name: "@Dynamic", value: header.Dynamic, direction: System.Data.ParameterDirection.Input);
                    parms.Add(name: "@IsDimension", value: header.IsDimension, direction: System.Data.ParameterDirection.Input);
                    parms.Add(name: "@Output", value: header.Output, direction: System.Data.ParameterDirection.Input);
                    header.Id = Context.ExecuteScalar<long>(sql: SQL.SaveHeader, param: parms, transaction: _currenttrans, commandTimeout: _timeout);
                }
            
            }
            Console.WriteLine(ic + "/" + ics + ", " + customerid + ", " + DateTime.Now.ToLongTimeString() + ", EANS: " + modeldata.EANS.Count);

            foreach (var ean in modeldata.EANS)
            {
                if (ean.Id == 0)
                {
                    parms = new DynamicParameters();
                    parms.Add(name: "@DataImportId", value: modeldata.Id, direction: System.Data.ParameterDirection.Input);
                    parms.Add(name: "@EAN", value: ean.Value, direction: System.Data.ParameterDirection.Input);
                    ean.Id = Context.ExecuteScalar<long>(sql: SQL.SaveEAN, param: parms, transaction: _currenttrans, commandTimeout: _timeout);
                }
                Console.WriteLine(ic + "/" + ics + ", " + customerid + ", " + DateTime.Now.ToLongTimeString() + ", Ean: " +ean.Value + ", Files: " + ean.Files.Count);

                foreach (var f in ean.Files)
                {
                    if (f.Id == 0)
                    {
                        parms = new DynamicParameters();
                        parms.Add(name: "@DataImportId", value: modeldata.Id, direction: System.Data.ParameterDirection.Input);
                        parms.Add(name: "@EanId", value: ean.Id, direction: System.Data.ParameterDirection.Input);
                        parms.Add(name: "@FileName", value: f.Value, direction: System.Data.ParameterDirection.Input);
                        parms.Add(name: "@InvoiceCount", value: f.Invoices.Keys.Count(), direction: System.Data.ParameterDirection.Input);
                        parms.Add(name: "@LineCount", value: f.Invoices.Values.Sum(r=>r.LineCount), direction: System.Data.ParameterDirection.Input);
                        f.Id = Context.ExecuteScalar<long>(sql: SQL.SaveFile, param: parms, transaction: _currenttrans, commandTimeout: _timeout);
                    }
                }
                Console.WriteLine(ic + "/" + ics + ", " + customerid + ", " + DateTime.Now.ToLongTimeString() + ", Ean: " + ean.Value + ", DimensionRows: " + ean.DimensionValues.Values.SelectMany(r=>r.Keys).Count());

                foreach (var d in ean.DimensionValues)
                {
                    foreach(var dv in d.Value)
                    {
                        if (string.IsNullOrWhiteSpace(dv.Value.DimensionCode.CodeName))
                        {
                            var t = 0;
                        }
                        parms = new DynamicParameters();
                        parms.Add(name: "@DataImportId", value: modeldata.Id, direction: System.Data.ParameterDirection.Input);
                        parms.Add(name: "@EanId", value: ean.Id, direction: System.Data.ParameterDirection.Input);
                        parms.Add(name: "@DimensionCodeId", value: d.Key.Id, direction: System.Data.ParameterDirection.Input);
                        parms.Add(name: "@DimensionValueId", value:dv.Key, direction: System.Data.ParameterDirection.Input);
                        parms.Add(name: "@DimensionCodeName", value: dv.Value.DimensionCode.CodeName, direction: System.Data.ParameterDirection.Input);
                        parms.Add(name: "@DimensionValueName", value: dv.Value.Name, direction: System.Data.ParameterDirection.Input);
                        parms.Add(name: "@DimensionValueValue", value: dv.Value.Value, direction: System.Data.ParameterDirection.Input);
                        parms.Add(name: "@Qty", value: dv.Value.Qty, direction: System.Data.ParameterDirection.Input);

                        Context.Execute(sql: SQL.SaveDimension, param: parms, transaction: _currenttrans, commandTimeout: _timeout);

                    }

                }

            }

          



        }
       
        public IDictionary<string, Header> GetHeaders(long importid)
        {
            var parms = new DynamicParameters();
            parms.Add(name: "@ImportId", value: importid, direction: System.Data.ParameterDirection.Input);
            return Context.Query<Header>(sql: SQL.GetHeaders, param: parms, transaction: _currenttrans, commandTimeout: _timeout).ToDictionary(r=>r.Name, r=>r);


        }
        public long GetDataRowCount(long importid)
        {
            var parms = new DynamicParameters();
            parms.Add(name: "@ImportId", value: importid, direction: System.Data.ParameterDirection.Input);
            return Context.Query<long>(sql: SQL.GetDataRowCount, param: parms, transaction: _currenttrans, commandTimeout: _timeout).FirstOrDefault();


        }

        public Tuple<int, IList<Model>, long> GetData(long importid, ModelData data, int batchsize, long lastid)
        {
            IList<Model> models = null;
            var parms = new DynamicParameters();
            parms.Add(name: "@ImportId", value: importid, direction: System.Data.ParameterDirection.Input);
            parms.Add(name: "@Batchsize", value: batchsize, direction: System.Data.ParameterDirection.Input);
            parms.Add(name: "@@LastId", value: lastid, direction: System.Data.ParameterDirection.Input);
            ILookup<long, FieldData> fields = null;

            models = Context.Query<Model>(sql: SQL.GetDataRow, param: parms, transaction: _currenttrans, commandTimeout: _timeout).ToList();
            fields = Context.Query<FieldData>(sql: SQL.GetDataCol, param: parms, transaction: _currenttrans, commandTimeout: _timeout).ToLookup(r => r.ModelId, r => r);

            var culture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            var styles = System.Globalization.DateTimeStyles.None;
            var workingdata = new List<Model>();
            foreach (var m in models)
            {
                var fielddatas = fields[m.Id];
                if (fielddatas.Any(r => r.DimensionValueId != 0 && data.Headers[r.HeaderName].IsDimension == true))
                {
                    foreach (var fielddata in fielddatas)
                    {

                        fielddata.Value = fielddata.Value.Replace(",", ".");

                        var header = data.GetHeader(fielddata.HeaderName);

                        fielddata.Value = fielddata.Value.Replace(",", ".");
                        if (header.Name == "IssueDate")
                        {

                            DateTime d;
                            if (DateTime.TryParse(fielddata.Value, culture, styles, out d))
                            {
                                fielddata.Value = d.ToString("dd/MM/yyyy HH:mm:ss").Replace(".", ":");
                            }
                            else
                            {

                            }
                        }
                        m.AddData(header, fielddata);
                        workingdata.Add(m);
                    }
                }
            }

            return new Tuple<int, IList<Model>, long>(models.Count, workingdata, models.Any() ? models.Last().Id: 0);
        }

        public void Close() { try { Context.Close(); } catch (Exception) { } }

        public void Dispose()
        {
            if (Context != null) Context.Dispose();
        }
    }
}
