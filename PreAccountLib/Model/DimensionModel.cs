﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreAccount_DataExtract
{
    public class DimensionModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime Changed { get; set; }
        public DateTime Created { get; set; }

        public IDictionary<string, DimensionCode> Codes { get; set; }
        public string OrganisationName { get; internal set; }
    }
    public class DimensionCode
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string CodeName { get; set; }
        public DateTime Changed { get; set; }
        public DateTime Created { get; set; }

        public bool Deleted { get; set; }
        public IDictionary<string,DimensionValue> Values { get; set; }
    }

    public class DimensionValue
    {
        public long Id { get; set; }
        public long DimensionCodeId { get; set; }
        public DimensionCode DimensionCode { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public long Qty { get; set; }

        public DateTime Changed { get; set; }
        public DateTime Created { get; set; }

        public bool Deleted { get; set; }

    }
}
