﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreAccount_DataExtract
{
    public class Split
    {
        public string Split0 { get; set; }
        public string Split1 { get; set; }
        public string Split2 { get; set; }

        public Model Model { get; set; }


        public DimensionModel DimensionModel { get; set; }
        public ModelData Data { get; set; }
        
        public EanData Eandata { get; set; }
        public bool Delayed { get; set; }
    }
}
