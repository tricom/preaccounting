﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
namespace PreAccount_DataExtract
{
    public class ModelData
    {
        public long Id { get; set; }
        public IDictionary<string, Header> Headers { get; set; }
        //public IList<Model> Data { get; set; }
        public int FileCount { get; internal set; }
        public IList<EanData> EANS { get; internal set; }
        public long DataRows { get; set; }

        public long DimensionModelId { get; set; }
        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public ModelData()
        {
            Headers = new Dictionary<string, Header>();
            EANS = new List<EanData>();
         //   Data = new List<Model>();
        }
        public bool HasHeader(string headername)
        {
            var h = Headers.ContainsKey(headername);
            if (!h)
            {
                foreach(var hd in Headers)
                {
                    if (hd.Key.Replace("-", "") == headername.Replace("-", ""))
                    {
                        h = true;
                        break;
                    }
                }

            }
            return h;
        }
        public Header GetHeader(string headername)
        {
            var h = Headers.ContainsKey(headername) ?  Headers[headername] : null;
            if (h == null)
            {
                foreach (var hd in Headers)
                {
                    if (hd.Key.Replace("-", "") == headername.Replace("-", ""))
                    {
                        h = hd.Value;
                        break;
                    }
                }

            }
            return h;
        }
        public void AddHeader(Header header)
        {
            Headers.Add(header.Name, header);
        }
        
    }

    public class EanData : IdObject
    {
        public List<FileData> Files { get; set; }
        public IDictionary<DimensionCode, IDictionary<long, DimensionValue>> DimensionValues { get; set; } 
       
        public EanData(string ean)
        {
            this.Value = ean;
            this.Files = new List<FileData>();
            this.DimensionValues = new Dictionary<DimensionCode,IDictionary<long, DimensionValue>>();
            
        }
    }
    public class FileData: IdObject
    {
        public IDictionary<string, InvoiceData> Invoices { get; set; }

        public  FileData(string filename)
        {
            this.Value = filename;
            this.Invoices = new Dictionary<string, InvoiceData>();
        }
    }
    public class InvoiceData
    {
        public string InvoiceId { get; set; }
        public long LineCount { get; set; }
       
    }
    public class IdObject
    {
        public long Id { get; set; }
        public string Value { get; set; }

        public IdObject(string value) { this.Value = value; }

        public IdObject() {}
    }
    public class Header
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool Dynamic { get; set; }
        public bool Output { get; set; }
        public bool IsDimension { get; set; }



        public Header(string name, bool output, bool dynamic)
        {
            this.Name = name;
            this.Dynamic = dynamic;
            this.Output = output;
        }
        public Header(string name) : this(name, true, false)
        {
        }
        public Header(string name, bool output) : this(name, output, false)
        {
        }
        public Header()
        {
        }
       
    }

    public class Model
    {
        public string AppKey { get; set; }
        public long Id { get; set; }

        public long DataImportId { get; set; }
        public IDictionary<Header, FieldData> Data { get; set; }
        public bool Deleted { get; internal set; }

        public Model()
        {
            Data = new Dictionary<Header, FieldData>();
            AppKey = System.Guid.NewGuid().ToString();
        }
        public void AddData(Header header, FieldData data)
        {
            if (!Data.ContainsKey(header)) Data.Add(header, data);
            else if (string.IsNullOrWhiteSpace(Data[header].Value)) Data[header] = data;
        }


        public string ToXml()
        {
            var s = new XElement("row", new XAttribute("DataImportId", DataImportId), new XAttribute("AppKey", AppKey)).ToString();
            return s;

        }

    }
    public class FieldData
    {
        public long ModelId { get; set; }
        public long HeaderId { get; set; }
        public string HeaderName { get; set; }
        public string Value{ get; set; }
        public long DimensionValueId { get; set; }

        public FieldData(string value, long dimensionvalueid)
        {
            this.Value = value;
            this.DimensionValueId = dimensionvalueid;

        }
        public FieldData(string value): this(value, 0)
        {

        }
        public FieldData() { }

        public string ToXml()
        {
            var s = new XElement("col", new XAttribute("rowid", ModelId), new XAttribute("headerid", HeaderId), new XAttribute("dimensionvalueid", DimensionValueId)
                , XTool.Create("Value", Value)
                ).ToString();
            return s;

        }
    }

    class XTool
    {
        public static XElement Create(string name, string value, int length)
        {
            if (string.IsNullOrEmpty(value)) return new XElement(name, value);
            return new XElement(name, new XCData((RemoveInvalidXmlChars(value) ?? "").Replace("]]>", "")));
        }

        public static XElement Create(string name, string value)
        {
            if (string.IsNullOrEmpty(value)) return new XElement(name, value);
            return new XElement(name, new XCData((RemoveInvalidXmlChars(value) ?? "").Replace("]]>", "")));
        }
        public static XElement Create(string name, byte? value)
        {
            return new XElement(name, new XText((value == null ? 0 : value.Value).ToString()));
        }

        public static XElement Create(string name, decimal? value)
        {
            return new XElement(name, new XText((value == null ? 0m : value.Value).ToString(System.Globalization.CultureInfo.InvariantCulture)));
        }

        public static XElement Create(string name, DateTime? value)
        {
            return new XElement(name, new XText((value == null ? new DateTime(1970, 01, 01) : value.Value).ToString("yyyy-MM-dd")));
        }

        public static XElement Create(string name, long value)
        {
            return new XElement(name, new XText(value.ToString()));
        }

        public static XElement Create(string name, bool value)
        {
            return new XElement(name, new XText(value.ToString()));
        }

        public static string RemoveInvalidXmlChars(string content)
        {
            return new string(content.Where(ch => System.Xml.XmlConvert.IsXmlChar(ch)).ToArray());
        }
    }
}
