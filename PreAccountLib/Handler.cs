﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tricom.Tricommerce.Services.FileAdapter;
using PreAccount_DataExtract;
namespace PreAccountLib
{
    public class Handler
    {
        public void HandleAll(string outputpath, string inputfolder, bool includedeleted, bool useDB)
        {
            HandleSpecific(outputpath, inputfolder, includedeleted, CustomerIds, useDB);

        }
        public void HandleSpecific(string outputpath, string inputfolder, bool includedeleted, IList<long> customerids, bool useDB)
        {
            //Check validity.
            foreach (var c in customerids) if (!CustomerIds.Any(r => r == c)) throw new ArgumentException();

            var ic = 0;

            foreach (var customerid in customerids)
            {

                Console.WriteLine(customerid + ", " + DateTime.Now.ToLongTimeString() + ", " + ic + "/" + customerids.Count);
                var csv = new CSV();
                csv.INCLUDEDELETED = includedeleted;
                csv.Outputfolder = outputpath;

                var modeldata = new ModelData();
                modeldata.Start = DateTime.Now;

                DimensionModel dimensionmodel = null;

                try
                {
                    using (var context = DBIndfak.ConnectionFactory())
                    {
                        context.Open();
                        Console.WriteLine(ic + "/" + customerids.Count + ", " + customerid + ", " + DateTime.Now.ToLongTimeString() + ", GetData");
                        try
                        {
                            dimensionmodel = context.GetDimensionModel(customerid);
                            modeldata.EANS = context.GetEANs(customerid).Select(r => new EanData(r)).ToList();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(ic + "/" + customerids.Count + ", " + customerid + ", " + DateTime.Now.ToLongTimeString() + ", " + e.Message + ", " + e.StackTrace);
                            throw;
                        }
                        finally
                        {

                            context.Close();
                        }
                    }

                    modeldata.DimensionModelId = dimensionmodel.Id;
                    AddHeaders(modeldata);

                    foreach (var c in dimensionmodel.Codes.Keys)
                    {
                        var h = new Header(c) { IsDimension = true, Output = includedeleted || !dimensionmodel.Codes[c].Deleted };
                        modeldata.AddHeader(h);
                    }
                    csv.StartDocument(customerid);

                    var ec = 0;
                    var continuewrite = true;
                    foreach (var e in modeldata.EANS)
                    {
                        if (!continuewrite) break;
                        ec++;
                        var path = inputfolder;
                        var files = FileUtils.ScanDirectory(path, string.Format("{0}_export.csv", e.Value));
                        modeldata.FileCount = files.Count();
                        Console.WriteLine(ic + "/" + customerids.Count + ", " + customerid + ", " + DateTime.Now.ToLongTimeString() + ", " + e.Value + ", " + "Found files: " + modeldata.FileCount + ", " + ec + " / " + modeldata.EANS.Count);

                        var fi = 0;
                        foreach (var f in files)
                        {
                            var filedata = new FileData(f.FullName);
                            e.Files.Add(filedata);
                            fi++;
                            Console.WriteLine(ic + "/" + customerids.Count + ", " + customerid + ", " + DateTime.Now.ToLongTimeString() + ", " + e.Value + ", " + fi + " / " + modeldata.FileCount + ", " + ec + " / " + modeldata.EANS.Count);
                            var models = csv.Handle(f, modeldata, dimensionmodel, e, filedata);
                            continuewrite = csv.WriteLine(models, modeldata);
                            models = null;
                            if (!continuewrite) break;



                            GC.Collect();
                        }
                    }

                    csv.CloseDocument(modeldata, customerid);


                    if (useDB)
                    {
                        modeldata.End = DateTime.Now;
                        using (var contextlocal = DBLocal.ConnectionFactory())
                        {
                            contextlocal.Open();
                            contextlocal.SaveData(ic, customerids.Count, customerid, null, modeldata, dimensionmodel);
                            contextlocal.Close();
                        }
                    }

                    ic++;

                }
                catch (Exception ex)
                {

                    Console.WriteLine(ic + "/" + customerids.Count + ", " + customerid + ", " + DateTime.Now.ToLongTimeString() + ", " + ex.Message + ", " + ex.StackTrace);

                }

                Console.WriteLine(ic + "/" + customerids.Count + ", " + customerid + ", " + DateTime.Now.ToLongTimeString() + ", " + "Done"); ;
            }
        }


        private void AddHeaders(ModelData modeldata)
        {
            modeldata.AddHeader(new Header("Filename", false));
            modeldata.AddHeader(new Header("EAN"));
            modeldata.AddHeader(new Header("Type"));
            modeldata.AddHeader(new Header("IbisticId", false));
            modeldata.AddHeader(new Header("Indfak2Id", false));
            modeldata.AddHeader(new Header("InvoiceId"));
            modeldata.AddHeader(new Header("InvoiceNum"));
            modeldata.AddHeader(new Header("InvoiceTotal"));
            modeldata.AddHeader(new Header("InvoiceTotalCurrency"));
            modeldata.AddHeader(new Header("LineIndex"));
            modeldata.AddHeader(new Header("IssueDate"));
            modeldata.AddHeader(new Header("UNSPSC"));
            modeldata.AddHeader(new Header("Name"));
            modeldata.AddHeader(new Header("Description"));
            modeldata.AddHeader(new Header("ItemNo"));
            modeldata.AddHeader(new Header("SupplierId"));
            modeldata.AddHeader(new Header("SupplierIdScheme"));
            modeldata.AddHeader(new Header("SupplierName"));
            modeldata.AddHeader(new Header("SupplierBranche1"));
            modeldata.AddHeader(new Header("SupplierBranche2"));
            modeldata.AddHeader(new Header("SupplierBranche3"));
            modeldata.AddHeader(new Header("Quantity"));
            modeldata.AddHeader(new Header("QuantityUnit"));
            modeldata.AddHeader(new Header("UnitPrice"));
            modeldata.AddHeader(new Header("UnitPriceCurrency"));
            modeldata.AddHeader(new Header("LineNote"));
            modeldata.AddHeader(new Header("InvoiceNote"));
            modeldata.AddHeader(new Header("CustomerName"));
            modeldata.AddHeader(new Header("CustomerBranche1"));
            modeldata.AddHeader(new Header("CustomerBranche2"));
            modeldata.AddHeader(new Header("CustomerBranche3"));
        }

        public IList<long> CustomerIds = new List<long>() {221454,
            221455,
            221454,
            221925,
            12268,
            12269,
            12287,
            14031,
            14032,
            14033,
            14034,
            14036,
            14037,
            14038,
            14041,
            20228,
            20229,
            20230,
            20231,
            20232,
            20233,
            20234,
            20235,
            20237,
            20238,
            20239,
            20240,
            20241,
            221093,
            221095,
            221096,
            221098,
            221099,
            221100,
            221102,
            221103,
            221104,
            221105,
            221121,
            221123,
            221124,
            221125,
            221127,
            221129,
            221131,
            221132,
            221135,
            221136,
            221137,
            221138,
            221139,
            221140,
            221142,
            221143,
            221144,
            221145,
            221146,
            221147,
            221148,
            221151,
            221156,
            221158,
            221159,
            221160,
            221161,
            221162,
            221163,
            221164,
            221180,
            221181,
            221182,
            221183,
            221184,
            221185,
            221186,
            221188,
            221189,
            221190,
            221193,
            221194,
            221195,
            221196,
            221200,
            221202,
            221203,
            221204,
            221207,
            221208,
            221222,
            221227,
            221228,
            221230,
            221232,
            221233,
            221235,
            221236,
            221237,
            221238,
            221239,
            221240,
            221241,
            221242,
            221243,
            221244,
            221245,
            221246,
            221247,
            221249,
            221250,
            221267,
            221268,
            221272,
            221273,
            221274,
            221275,
            221277,
            221278,
            221279,
            221280,
            221282,
            221283,
            221285,
            221286,
            221287,
            221289,
            221290,
            221291,
            221292,
            221305,
            221306,
            221307,
            221310,
            221311,
            221319,
            221320,
            221321,
            221322,
            221323,
            221333,
            221334,
            221335,
            221336,
            221337,
            221338,
            221339,
            221340,
            221341,
            221342,
            221356,
            221357,
            221358,
            221359,
            221360,
            221361,
            221362,
            221364,
            221365,
            221367,
            221379,
            221380,
            221382,
            221385,
            221387,
            221388,
            221397,
            221398,
            221399,
            221400,
            221401,
            221402,
            221403,
            221404,
            221405,
            221406,
            221407,
            221408,
            221409,
            221410,
            221411,
            221412,
            221413,
            221450,
            221452,
            221465,
            221466,
            221470,
            221471,
            221472,
            221475,
            221477,
            221479,
            221487,
            221488,
            221492,
            221501,
            221502,
            221503,
            221504,
            221505,
            221506,
            221507,
            221819,
            221827,
            221829,
            221832,
            221855,
            221859,
            221867,
            221868,
            221869,
            221875,
            221877,
            221878,
            221894,
            221914,
            222018,
            222028,
            222038,
            222045,
            222202,
            222215,
            222216,
            222233,
            222234,
            222236,
            222241,
            222265,
            222271,
            222278,
            222280,
            222501,
            222503,
            222505,
            222507,
            222514,
            222518,
            222526,
            222537,
            222667,
            222668,
            224325,
            224390,
            224450,
            228053,
            228381,
            228382,
            228383,
            228384,
            228385,
            228386,
            228387,
            228388,
            228389,
            228390,
            228391,
            228392,
            228393,
            228394,
            228395,
            228396,
            228397,
            228398,
            228399,
            228400,
            228401,
            228403,
            228404,
            228407,
            228410,
            228411,
            228412,
            228475,
            228477,
            228519,
            228529,
            228565,
            228573,
            228586,
            228604,
            30816,
            30817,
            31634,
            31635,
            31636,
            31637,
            31638,
            31639,
            31640,
            31642,
            31643,
            31644,
            31645,
            31646,
            31647,
            31648,
            31649,
            31651

        };

    }
}
