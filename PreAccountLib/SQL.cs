﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreAccount_DataExtract
{
    public class SQL
    {
        public const string GetEANs  = @"SELECT ean FROM xxOrgEANs WHERE orgid = @customerid";
        
        public const string GetDimensionModelData = @"SELECT Id, name, changed, created FROM dbo.DimensionModels WHERE id = (SELECT top 1 dimid FROM dbo.xxOrgEANs WHERE Orgid = @customerid) AND Deleted =0;                            
                                                      SELECT Id, Name, CodeName,  Changed, Created, CASE WHEN Deleted = 1 OR NOT EXISTS(SELECT TOP 1 1 FROM dbo.DimensionValues dv WHERE dv.DimensionCodeId = dc.id AND dv.deleted=0) Then 1 else 0 end as Deleted FROM dbo.DimensionCodes dc WHERE DimensionModelId = (SELECT top 1 dimid FROM dbo.xxOrgEANs WHERE Orgid = @customerid) AND dc.CodeName IS NOT null and dc.CodeName!='Alias' AND EXISTS(SELECT TOP 1 1 FROM dbo.DimensionValues dv WHERE dv.DimensionCodeId = dc.id);
                                                      SELECT Id, DimensionCodeId, Name, Value, Deleted, Changed, Created FROM dbo.DimensionValues WHERE DimensionCodeId IN (SELECT id FROM dimensioncodes WHERE DimensionModelId =  (SELECT top 1 dimid FROM dbo.xxOrgEANs WHERE Orgid = @customerid) AND Deleted =0 AND CodeName IS NOT null  and CodeName!='Alias') ;                                                                
                                                      Select name from organisationbase where id = @customerid;
                                                      ";
        public const string SaveRun = @"INSERT INTO [dbo].[DataImport]([CustomerId],[OrgName], [Start],[End],[DimensionModelId]) VALUES (@CustomerId,@OrgName, @Start,@End, @DimensionModelId);SELECT SCOPE_IDENTITY()";
        public const string SaveHeader = @"INSERT INTO [dbo].[DataImport_Header] ([DataImportId],[Name],[Dynamic],[Output],[IsDimension]) VALUES (@DataImportId, @Name,@Dynamic, @output, @IsDimension);SELECT SCOPE_IDENTITY()";


        public const string SaveEAN = @"INSERT INTO [dbo].[DataImport_EAN] ([DataImportId],[EAN]) VALUES (@DataImportId, @EAN);SELECT SCOPE_IDENTITY()";
        public const string SaveFile = @"INSERT INTO [dbo].[DataImport_File] ([DataImportId], [EanId],[FileName],[InvoiceCount], [LineCount]) VALUES (@DataImportId, @EanId, @FileName, @InvoiceCount, @LineCount);SELECT SCOPE_IDENTITY()";



        public const string SaveDimension = @"INSERT INTO [dbo].[DataImport_Dimensions]([DataImportId],[EanId],[DimensionCodeId],[DimensionValueId],[DimensionCodeName],[DimensionValueName],[DimensionValueValue], [Qty]) VALUES(@DataImportId,@EanId, @DimensionCodeId,@DimensionValueId, @DimensionCodeName,@DimensionValueName,@DimensionValueValue, @Qty)";


        public const string SaveRows = @"DECLARE @data XML = @sData 
                                         DECLARE @result TABLE (id bigint, appkey nvarchar(MAX))
                                        INSERT INTO dbo.DataImport_Row(AppKey,DataImportId,Created)
                                        OUTPUT inserted.id,Inserted.AppKey INTO @result
                                        SELECT	 txml.x.value('@AppKey',N'nvarchar(50)') AS AppKey
		                                        ,txml.x.value('@DataImportId',N'bigint') AS DataImportId
		                                        ,GETDATE()
                                        FROM @data.nodes('//row') as txml(x)
	                                    SELECT Id, Appkey AS Value FROM @result
                                        ";

        public const string SaveCols = @"DECLARE @data XML = @sData 
                                        INSERT INTO dbo.DataImport_Col
                                        (
                                            DataImport_Row_Id,
                                            DataImport_Header_Id,
                                            Value,
                                            DimensionValueId,
                                            Created
                                        )
                                        SELECT	 txml.x.value('@rowid',N'bigint') AS rowid
		                                         ,txml.x.value('@headerid',N'bigint') AS headerid
		                                         ,txml.x.value('(Value/text())[1]',N'nvarchar(max)') AS Value  
		                                         ,txml.x.value('@dimensionvalueid',N'bigint') AS dimensionvalueid
		                                        , GETDATE()
                                        FROM @data.nodes('//col') as txml(x)
                                        WHERE txml.x.value('(Value/text())[1]',N'nvarchar(max)') IS NOT null";
        public const string SaveRow = @"INSERT INTO[dbo].[DataImport_Row]([DataImportId],[Created]) VALUES(@DataImportId, GETDATE())   ;SELECT SCOPE_IDENTITY()";
        public const string SaveCol = @"INSERT INTO [dbo].[DataImport_Col] ([DataImport_Row_Id],[DataImport_Header_Id],[Value],[DimensionValueId],[Created]) VALUES (@DataImport_Row_Id,@DataImport_Header_Id,@Value, @DimensionValueId,GETDATE())";
        internal static readonly string GetDataRow = @"SELECT TOP (@batchsize) Id FROM dbo.DataImport_Row WHERE dataimportid = @ImportId AND Id>@LastId order by id";
        internal static readonly string GetDataCol = @"SELECT dc.Value, dc.dataimport_row_id AS ModelId, header.Name AS HeaderName, dc.dimensionvalueid FROM dataimport_col dc
                                                    INNER JOIN dataimport_header AS header ON header.Id = dc.dataimport_header_id
                                                    WHERE dc.dataimport_row_id IN (
                                                        SELECT TOP (@batchsize) id FROM dbo.DataImport_Row WHERE dataimportid = @ImportId AND Id>@LastId order by id
                                                    );";
        internal static readonly string GetHeaders = @"SELECT * FROM dbo.DataImport_Header WHERE dataimportid = @ImportId order by id";
        internal static readonly string GetDataRowCount = @"SELECT COUNT(*) AS i FROM dbo.DataImport_Row WHERE dataimportid = @ImportId";
    }
}
