﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using System.Globalization;

using System.IO.Compression;
namespace PreAccount_DataExtract
{
    public class CSV
    {
        public bool INCLUDEDELETED;
        Dictionary<string, object> _hasfound = new Dictionary<string, object>();
        IDictionary<int, Header> filesetup = new Dictionary<int, Header>();
        public string Outputfolder;
        public StreamWriter _sw { get; private set; }
        public CsvWriter _writer { get; private set; }

        CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
        DateTimeStyles styles = DateTimeStyles.None;

        internal IList<Model> Handle(FileSystemInfo f, ModelData data,DimensionModel dm,  EanData eandata, FileData filedata)
        {
            filesetup.Clear();

            var firstline = string.Empty;

            using (FileStream fs = new FileStream(f.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (StreamReader sr = new StreamReader(fs))
                {
                    if (!sr.EndOfStream)
                    {
                        firstline = sr.ReadLine();
                    }
                }
            }

            var delimiter = firstline.Split(',').Length > firstline.Split(';').Length ? "," : ";";
           
            IDictionary<string,IList<Model>> models = new Dictionary<string, IList<Model>>();
            TextReader reader = new StreamReader(f.FullName);
            var csvReader = new CsvReader(reader, new System.Globalization.CultureInfo("da-DK"));
            csvReader.Configuration.Encoding = System.Text.Encoding.UTF8;
            csvReader.Configuration.HasHeaderRecord = true;
            csvReader.Configuration.IgnoreBlankLines = true;
            csvReader.Configuration.Delimiter = delimiter;
            csvReader.Configuration.BadDataFound = null;
            
            string value;
            string invoiceid = string.Empty;
            var row = 0;
            while (csvReader.Read())
            {
                var splits = new List<Split>();
                var model = new Model();
                var current_sag = string.Empty;
           
                model.Data.Add(data.GetHeader("Filename"), new FieldData(f.FullName));

                var deleted = false;
                for (int col = 0; csvReader.TryGetField<string>(col, out value); col++)
                {
                    if (row == 0)
                    {
                        Header header = null;
                        if (value.ToLower() == "codingstring")
                        {
                            foreach (var c in dm.Codes.Keys)
                            {                                
                                header = data.GetHeader(c);
                                if (!filesetup.ContainsKey(col)) filesetup.Add(col, header);
                            }
                        }
                        else
                        {
                            header = data.GetHeader(value);
                            if (!filesetup.ContainsKey(col)) filesetup.Add(col, header);
                        }
                    }
                    else
                    {
                        try
                        {
                            Header header = null;
                            
                            
                          
                            if (filesetup[col].Name == "InvoiceId")
                            {
                                if (!filedata.Invoices.ContainsKey(value)) filedata.Invoices.Add(value, new InvoiceData());
                                filedata.Invoices[value].InvoiceId = value;
                                filedata.Invoices[value].LineCount++;
                                invoiceid = value;
                            }

                            if (filesetup[col].IsDimension)
                            {
                                var splitdata = value.Split(new string[1] { "," }, StringSplitOptions.None);

                                
                                for (int j = 0; j<splitdata.Length;j= j+3)
                                {
                                    var split = new Split();
                                    split.Split0 = splitdata[j];
                                    split.Split1 = splitdata[j+1];
                                    split.Split2 = splitdata[j + 2];
                                    split.DimensionModel = dm;
                                    split.Data = data;
                                    split.Model = model;
                                    split.Eandata = eandata;
                                    HandleDimension(split, true, string.Empty);

                                    splits.Add(split);


                                }                              
                            }
                            else
                            {
                                var fielddata = new FieldData(string.Empty, 0);
                                fielddata.Value = value;
                                header = filesetup[col];
                                model.AddData(header, fielddata);
                            }

                          
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error");
                            throw;
                        }

                    }
                }
                 
                if (row > 0)// && (INCLUDEDELETED || !deleted))
                {
                    var lastvalue = string.Empty;
                    foreach (var split in splits.Where(r => r.Delayed).Reverse())
                    {
                        lastvalue = HandleDimension(split, false, string.IsNullOrEmpty(lastvalue) ? string.Empty : lastvalue + ",");
                    }
                    if (!models.ContainsKey(invoiceid)) models.Add(invoiceid, new List<Model>());


                    model.Deleted = deleted;                    
                    models[invoiceid].Add(model);
                }

                row++;
            }
            reader.Close();

            var resultmodels = new List<Model>();
            foreach(var k in models)
            {
                if (INCLUDEDELETED || !k.Value.Any(r => r.Deleted))
                {                    
                    resultmodels.AddRange(k.Value);
                }
              
            }


            data.DataRows += resultmodels.Count;

            
            return resultmodels;
        }


        private string HandleDimension(Split split, bool withdelay, string lastvalue)
        {
            var dcode = string.Empty;
            var dvalue = string.Empty;
            var deleted = false;
            Header header = null;

            dcode = split.Split1.Replace("\"", string.Empty);

            if (dcode.ToLower() == "finans") dcode = "GLAccount";
            else if (dcode.ToLower() == "anlæg") dcode = "FixedAsset";
            else if (dcode.ToLower() == "momsprodbogfgrp") dcode = "Vat";
            else if (dcode.ToLower() == "sagsopgave") dcode = "JobTask";
            else if (dcode.ToLower() == "sag") dcode = "Job";
            else if (dcode.ToLower() == "budget") dcode = "GLBudget";
            else if (dcode.ToLower() == "vare") dcode = "Item";

            if (withdelay  && (dcode == "Job" || dcode == "JobTask"))
            {
                split.Delayed = true;
            }
            else
            {
                dvalue = split.Split2.Replace("\"", string.Empty); ;
                dvalue = lastvalue + dvalue;
                DimensionCode dc = GetDimensionCode(split.DimensionModel, dcode);

                if (dc != null)
                {
                    deleted = deleted || dc.Deleted;
                    //if (!dc.Deleted)
                    {
                        if (dc.Values.ContainsKey(dvalue))
                        {
                            var dvname = dc.Values[dvalue].Name;
                            var fielddata = new FieldData(string.Empty, 0);
                            header = split.Data.GetHeader(dc.CodeName);
                            fielddata.Value = dvalue + " - " + dvname;
                            deleted = deleted || dc.Values[dvalue].Deleted;

                            fielddata.DimensionValueId = dc.Values[dvalue].Deleted ? 0 : dc.Values[dvalue].Id;

                            if (!dc.Values[dvalue].Deleted)
                            {
                                if (!split.Eandata.DimensionValues.ContainsKey(dc)) split.Eandata.DimensionValues.Add(dc, new Dictionary<long, DimensionValue>());
                                if (!split.Eandata.DimensionValues[dc].ContainsKey(dc.Values[dvalue].Id)) split.Eandata.DimensionValues[dc].Add(dc.Values[dvalue].Id, dc.Values[dvalue]);
                                split.Eandata.DimensionValues[dc][dc.Values[dvalue].Id].Qty++;
                            }
                            
                            split.Model.AddData(header, fielddata);
                        }
                        else
                        {
                            var t = dvalue;

                        }
                    }
                }
                else
                {
                    if (!_hasfound.ContainsKey(dcode))
                    {
                        Console.WriteLine("NOTFOUND: " + dcode);
                        var s = dcode;
                        _hasfound.Add(dcode, null);
                    }
                }
            }
            return dvalue;
        }

        private DimensionCode GetDimensionCode(DimensionModel dm, string dcode)
        {
            DimensionCode dc = null;
            if (dm.Codes.ContainsKey(dcode))
            {
                dc = dm.Codes[dcode];
                if (!dc.Deleted) return dc;
            }
            if (dc == null || dc.Deleted)
            {
                foreach (var d in dm.Codes)
                {
                    if (d.Key.Replace("-", "") == dcode.Replace("-", ""))
                    {                         
                        dc =  d.Value;
                    }
                }
            }

            return dc;
        }

    
        internal bool WriteLine(IList<Model> data, ModelData modeldata)
        {
            var result = true;
            foreach (var row in data)
            {
           
                try
                {
                    foreach (var h in modeldata.Headers.Values.Where(r => r.Output))
                    {
                        if (row.Data.ContainsKey(h))
                        {


                            if (h.IsDimension && row.Data[h].DimensionValueId == 0 && !string.IsNullOrEmpty(row.Data[h].Value))
                            {
                                var value = row.Data[h].Value;
                                value = value.Replace(",", ".");
                                _writer.WriteField(value + "¤¤");
                            }
                            else
                            {
                                var value = row.Data[h].Value;
                                value = value.Replace(",", ".");


                                if (h.Name == "IssueDate")
                                {
                                    DateTime d;
                                    if (DateTime.TryParse(value, culture, styles, out d))
                                    {
                                        value = d.ToString("dd/MM/yyyy HH:mm:ss").Replace(".", ":");
                                    }
                                    else
                                    {
                                        value = string.Empty;
                                    }

                                }

                                _writer.WriteField(value);
                            }
                        }
                        else _writer.WriteField(string.Empty);
                   }
                    _writer.NextRecord();
                }
                catch(Exception x)
                {
                    Console.WriteLine("Error");
                }
            }
            data = null;
            return result;
        }

        internal void CloseDocument(ModelData modeldata, long customerid)
        {
            string dir = string.Format(@"{0}\{1}", Outputfolder, customerid);
            string path = string.Format(@"{0}\{1}_temp.csv", dir, customerid);

            _sw.Close();


             var path2 = string.Format(@"{0}\{1}.csv", dir, customerid);
            // Console.WriteLine(ic + "/" + ics + ", " + customerid + ", " + DateTime.Now.ToLongTimeString() + ", " + "Writing: " + path);

            var sw2 = new StreamWriter(path2);
            var writer2 = new CsvWriter(sw2, new System.Globalization.CultureInfo("da-DK"));
            writer2.Configuration.HasHeaderRecord = true;
            writer2.Configuration.Delimiter = ",";

            foreach (var h in modeldata.Headers.Values.Where(r => r.Output))
            {
                writer2.WriteField(h.Name);
            }
            writer2.NextRecord();

            using (StreamReader sr = new StreamReader(path))
            {
                string row = string.Empty;
                while ((row = sr.ReadLine()) != null)
                {
                    sw2.WriteLine(row);
                }              
            }
            sw2.Close();

            
            string zippath = string.Format(@"{0}\{1}.zip", dir, customerid);

            using (FileStream fs = new FileStream(zippath, FileMode.Create))
            using (ZipArchive arch = new ZipArchive(fs, ZipArchiveMode.Create))
            {
                arch.CreateEntryFromFile(path2, string.Format("{0}.csv", customerid));
            }

            System.IO.File.Delete(path2);
            System.IO.File.Delete(path);
        }

        internal void StartDocument(long customerid)
        {
            string dir = string.Format(@"C:\temp\expdata\{0}", customerid);
            if (!System.IO.Directory.Exists(dir)) System.IO.Directory.CreateDirectory(dir);
            string path = string.Format(@"{0}\{1}_temp.csv", dir, customerid);
           // Console.WriteLine(ic + "/" + ics + ", " + customerid + ", " + DateTime.Now.ToLongTimeString() + ", " + "Writing: " + path);

            _sw = new StreamWriter(path);
            _writer = new CsvWriter(_sw, new System.Globalization.CultureInfo("da-DK"));
            _writer.Configuration.HasHeaderRecord = true;
            _writer.Configuration.Delimiter = ",";
        }

        private void ignore(ReadingContext obj)
        {
           
        }

     
    }
}
