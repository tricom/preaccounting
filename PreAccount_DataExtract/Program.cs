﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tricom.Tricommerce.Services.FileAdapter;
using PreAccountLib;
namespace PreAccount_DataExtract
{
    class Program
    {

        static void Main(string[] args)
        {
            var handler = new Handler();
            handler.HandleAll(outputpath: @"C:\temp\expdata", inputfolder: @"C:\Temp\export20200405\export20200405", includedeleted:false, useDB:false);
            handler.HandleSpecific(outputpath: @"C:\temp\expdata", inputfolder: @"C:\Temp\export20200405\export20200405\specificfolder", customerids: new List<long>() { 221455 }, includedeleted: false, useDB: false  );
        }
    }
}

